#include <iostream>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <fcntl.h>
#include <unistd.h>

#include "DatosMemCompartida.h"
#include "Esfera.h"
#include "Raqueta.h"

int main()
{
	int fd2;
	DatosMemCompartida *pdatos;
	char *atr;
	fd2=open("/tmp/datos.txt",O_RDWR);
	atr=(char*)mmap(NULL,sizeof(*(pdatos)),PROT_WRITE|PROT_READ,MAP_SHARED,fd2,0);
	close(fd2);
	pdatos=(DatosMemCompartida*)atr;
	
	while(1)
	{	
		usleep(25000);  
		float posRaqueta;
		posRaqueta=((pdatos->raqueta1.y2+pdatos->raqueta1.y1)/2);
		if(posRaqueta<pdatos->esfera.centro.y)
			pdatos->accion=1;
		else if(posRaqueta>pdatos->esfera.centro.y)
			pdatos->accion=-1;
		else
			pdatos->accion=0;
	}
	munmap(atr,sizeof(*(pdatos)));	
}
