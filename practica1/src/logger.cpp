#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <stdio.h>
#include <fcntl.h>

int main(int argc,char *argv[])
{
	mkfifo("/tmp/mififo",0777);		//creacion tuberia
	int fd=open("/tmp/mififo",O_RDONLY); 	//abrir tuberia solo lectura
	char buffer[100];

	while(read(fd,buffer,sizeof(buffer)))
	{
		printf("%s\n",buffer);
	}
	close(fd);
	unlink("/tmp/mififo");
}
